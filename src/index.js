import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as firebase from 'firebase';
import registerServiceWorker from './registerServiceWorker';

var config = {
    apiKey: "AIzaSyAMZcBBU9xXd3Supyc0YgJvtgB3bkZiL3A",
    authDomain: "react-firebase-sample-e042b.firebaseapp.com",
    databaseURL: "https://react-firebase-sample-e042b.firebaseio.com",
    projectId: "react-firebase-sample-e042b",
    storageBucket: "react-firebase-sample-e042b.appspot.com",
    messagingSenderId: "1083985583207"
};

firebase.initializeApp(config);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
