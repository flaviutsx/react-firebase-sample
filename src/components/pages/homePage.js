import React, { Component } from 'react';
import * as firebase from 'firebase';

class Homepage extends Component {

  constructor() {
    super();
    this.state = {
      speed: ''
    };
  }

  componentDidMount() {
      const rootRef = firebase.database().ref().child('react');
      const speedRef = rootRef.child('speed');
      speedRef.on('value', snap => {
        this.setState({
          speed: snap.val()
        }); 
      });
  }

  render() {
    return (
      <div className="container-fluid">
        <h1>Homepage</h1>
        <p>
        {this.state.speed}
        </p>
      </div>
    );
  }
}

export default Homepage;
