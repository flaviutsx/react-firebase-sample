import React, { Component } from 'react';
import * as firebase from 'firebase';

class Productspage extends Component {

  constructor() {
    super();
    this.state = {
      text: ''
    };
  }

  componentDidMount() {
      const rootRef = firebase.database().ref().child('react');
      const textRef = rootRef.child('speed');
      textRef.on('value', snap => {
        this.setState({
          text: snap.val()
        }); 
      });
  }

  render() {
    return (
      <div className="container-fluid">
        <h1>Products</h1>
        <p>
        {this.state.text}
        </p>
      </div>
    );
  }
}

export default Productspage;
